package com.qa.newbie.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;

public class DashboardPageTest {
	ExtentReports extent;
	WebDriver driver;
	
	//Create OT/Page Object -> using by locator
	
	By textDashboard = By.xpath("/html/body/div/div[1]/div[1]/header/div[1]/div[1]/span/h6");
	
	// Define Constructor
	
	public void homePage (WebDriver driver, ExtentReports extent) {
		this.driver = driver;
		this.extent = extent;
	}
	
	
}
