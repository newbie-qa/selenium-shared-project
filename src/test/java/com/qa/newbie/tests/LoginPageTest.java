package com.qa.newbie.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.gherkin.model.And;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Given;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.aventstack.extentreports.gherkin.model.Then;
import com.aventstack.extentreports.gherkin.model.When;
import com.qa.newbie.config.ReportManager;
import com.qa.newbie.config.WebDriverManager;
import com.qa.newbie.pages.*;
import com.qa.newbie.util.Constant;

public class LoginPageTest {

	ExtentReports extent = null;
	ExtentTest feature = null;
	ExtentTest scenario = null;
	ExtentTest test = null;
	ExtentTest step = null;
	ExtentTest node = null;
	WebDriver driver;
	LoginPage login;
	DashboardPage dashboard;
	int timeout = 10;
	int sleep = 10;

	@BeforeSuite
	public void setupSuite(){
		extent = ReportManager.reportSetup();
		//extent.setAnalysisStrategy(AnalysisStrategy.TEST);
		// feature
		node = extent.createTest("Check Status TestNG");
		feature = extent.createTest(Feature.class, "Test Login");
		node.info("Before Suite : Setup Suite Complete");
	}

	@BeforeTest
	public void setupTest(){
		try {
			WebDriverManager.chromeDriver();
			node.info("Before Test : Setup Test Complete");
		} catch (Exception e) {
			node.fail(e);
		}
	}

	@BeforeMethod
	public void setupMethod(){
		try {
			driver = new ChromeDriver();
			driver.get(Constant.PAGEURL);
			login = new LoginPage(driver, extent);
			dashboard = new DashboardPage(driver, extent);
			node.info("Before Test : Setup Method Complete");
		} catch (Exception e) {
			node.fail("Setup Method : "+e);
		}
	}
	@Test
	public void loginTest01() {
		// scenario
		scenario = feature.createNode(Scenario.class, 
				"Verify if a user able login with valid username and password");
		scenario.addScreenCaptureFromBase64String("base64String");
		// step
		try {
			step = scenario.createNode(Given.class, 
					"A user input valid username");
			login.verifyUsername(timeout, sleep)
			.setUsername(Constant.USERNAME);
			step.pass("Pass", 
					MediaEntityBuilder.createScreenCaptureFromBase64String("base64String").build());

			step = scenario.createNode(And.class, 
					"A user input valid password");
			login.verifyPassword(timeout, sleep)
			.setPassword(Constant.PASSWORD);
			step.pass(Constant.PASS);

			step = scenario.createNode(When.class, 
					"A user click Login Button");
			login.verifyBtnLogin(timeout, sleep)
			.clickBtnLogin();
			step.pass(Constant.PASS);

			step = scenario.createNode(Then.class, 
					"A user should be able Login and direct to Dashboard Page");
			dashboard.verifyTextDashboard(timeout, sleep)
			.assertTextDashboard();
			step.pass(Constant.PASS);

			node.info("Run Test : Test Complete");
		} catch (Exception e) {
			step.fail("Test01 : "+e);
			node.fail("Test01 : "+e);
		}
		//scenario = feature.createNode(Scenario.class, "Verify if the login page for both, when test field is blank and Submit button is clicked");
	}

	@Test
	public void loginTest02() {
		// scenario
		scenario = feature.createNode(Scenario.class, 
				"Verify if a user cannot login with a invalid username and password");
		// step
		try {
			step = scenario.createNode(Given.class, 
					"A user input valid username");
			login.verifyUsername(timeout, sleep)
			.setUsername(Constant.INVALIDUSERNAME);
			step.pass(Constant.PASS);

			step = scenario.createNode(And.class, 
					"A user input valid password");
			login.verifyPassword(timeout, sleep)
			.setPassword(Constant.INVALIDPASSWORD);
			step.pass(Constant.PASS);

			step = scenario.createNode(When.class, 
					"A user click Login Button");
			login.verifyBtnLogin(timeout, sleep)
			.clickBtnLogin();
			step.pass(Constant.PASS);

			step = scenario.createNode(Then.class, 
					"A user should be able Login and direct to Dashboard Page");
			login.verifyAlertCredential(timeout, sleep);
			try {
				dashboard.verifyTextDashboard(timeout, sleep);
				step.fail(Constant.FAIL);
			} catch (Exception e) {
				step.pass(Constant.PASS);
			}
			node.info("Run Test : Test Complete");
		} catch (Exception e) {
			step.fail("Test02 : "+e);
			node.fail("Test02 : "+e);
		}
	}
	@AfterMethod
	public void closeMethod() {
		try {
			driver.close();	
			node.info("After Method : Close Method Complete");
		} catch (Exception e) {
			node.fail(e);
		}
	}

	@AfterSuite
	public void tearDown() {
		try {
			driver.quit();
			node.info("After Test : TearDown Complete");
			extent.flush();
		} catch (Exception e) {
			node.fail(e);
			extent.flush();
		}
	}
}
