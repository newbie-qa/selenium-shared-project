package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class login {

	private static WebElement element = null;
	private static WebDriverWait wait = null;
	
	public static WebElement textboxUsername (WebDriver driver){
		wait = new WebDriverWait(driver, 10, 500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("username")));
		element = driver.findElement(By.name("username"));
		return element;
	}
	
	public static WebElement textboxPassword (WebDriver driver){
		wait = new WebDriverWait(driver, 10, 500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("password")));
		element = driver.findElement(By.name("password"));
		return element;
	}
}
