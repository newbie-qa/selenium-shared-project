import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

import pages.*;
//import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.opera.OperaDriver;
//import org.openqa.selenium.safari.SafariDriver;

public class BrowserTest {

	static String url = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";
	static WebDriver driver = null;
	static WebElement username = null;
	static WebDriverWait wait = null;
	
//	public static void main(String[] args) throws InterruptedException {
//		setPathDriver();
//		openBrowserChrome();
//		login();
//		//clean();
//	}

	public static void setPathDriver() {
		String driversPath = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", driversPath+"\\drivers\\chromedriver\\chromedriver.exe");
		//System.setProperty("webdriver.gecko.driver", driversPath+"\\drivers\\geckodriver\\geckodriver.exe");
		//System.setProperty("webdriver.edge.driver", driversPath+"\\drivers\\edgedriver\\msedgedriver.exe");
		//System.setProperty("webdriver.ie.driver", driversPath+"\\drivers\\iedriver\\iedriver.exe");
		//System.setProperty("webdriver.opera.driver", driversPath+"\\drivers\\operadriver\\operadriver.exe");
		//System.setProperty("webdriver.safari.driver", driversPath+"\\drivers\\safaridriver\\safaridriver.exe");
	}
	
	public static void openBrowserChrome() throws InterruptedException {
		driver = new ChromeDriver();
		//driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 30, 500);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//input[@name='username']")));
		driver.get(url);
		
		
	}
	
	public static void login() {
		login.textboxUsername(driver).sendKeys("Admin");
	}
	
	public static void clean( ) {
		driver.close();
		driver.quit();
	}
	
	public static void openBrowserFirefox(WebDriver driver, String note) throws InterruptedException {
		driver = new ChromeDriver();
		//driverFirefox = new FirefoxDriver();
		//driverEdge = new EdgeDriver();
		//driverIE = new InternetExplorerDriver();
		//driverOpera = new OperaDriver();
		//driverSafari = new SafariDriver();
		
		driver.get(url);
		
		username = driver.findElement(By.name("username"));
		Assert.assertEquals(true, username.isDisplayed());
		
		//driver.findElement(By.name("username")).sendKeys("Admin");
		//login.textBoxUsername(driver).sendKeys("Admin");
		driver.close();
		driver.quit();
		System.out.println(note);
	}
}
