package com.qa.newbie.config;

public class WebDriverManager {

	private static String driversPath = System.getProperty("user.dir");
	
	public static void chromeDriver() {
		System.setProperty("webdriver.chrome.driver", driversPath+"\\drivers\\chromedriver\\chromedriver.exe");
	}
	
	public static void firefoxDriver() {
		System.setProperty("webdriver.gecko.driver", driversPath+"\\drivers\\geckodriver\\geckodriver.exe");
	}
	
	public static void ieDriver() {
		System.setProperty("webdriver.ie.driver", driversPath+"\\drivers\\iedriver\\iedriver.exe");
	}
	
	public static void edgeDriver() {
		System.setProperty("webdriver.edge.driver", driversPath+"\\drivers\\edgedriver\\msedgedriver.exe");
	}
	
	public static void operaDriver() {
		System.setProperty("webdriver.opera.driver", driversPath+"\\drivers\\operadriver\\operadriver.exe");
	}
	
	public static void safariDriver() {
		System.setProperty("webdriver.safari.driver", driversPath+"\\drivers\\safaridriver\\safaridriver.exe");
	}

}
