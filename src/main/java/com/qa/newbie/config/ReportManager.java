package com.qa.newbie.config;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ReportManager {

	private static String url = "reports/extendReports"+System.currentTimeMillis()+".html";
	static ExtentReports extent;
	static ExtentSparkReporter sparkReporter;

	public static ExtentReports reportSetup() {
		extent = new ExtentReports();
		sparkReporter = new ExtentSparkReporter(url);
		extent.attachReporter(sparkReporter);
		return extent;
	}
	
}
