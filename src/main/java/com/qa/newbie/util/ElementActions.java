package com.qa.newbie.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ElementActions {

	WebDriver driver;
	private static WebDriverWait wait;
	
	public ElementActions(WebDriver driver) {
		this.driver = driver;
	}
	
	public void verifyELementVisible(By path, int timeout, int sleep) {
		wait = new WebDriverWait(driver, timeout, sleep);
		wait.until(ExpectedConditions.visibilityOfElementLocated(path));
	}
	
	public ElementActions doSendKey(By path, String key) {
		driver.findElement(path).sendKeys(key);
		return this;
	}
	
	public ElementActions doClick(By path) {
		driver.findElement(path).click();
		return this;
	}
	
	public ElementActions verifyObjectByElement(By path, String text) {
		Assert.assertEquals(text, driver.findElement(path).getText());
		return this;
	}

	public void verifyObject(By path, String text) {
		Assert.assertEquals(text, driver.findElement(path).getText());
	}
}
