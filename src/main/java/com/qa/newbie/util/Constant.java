package com.qa.newbie.util;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class Constant {
	// Config
	public static final String PAGEURL = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";
	public static final String SCREENCAPTURE = "SS"+System.currentTimeMillis()+".png";
	public static final String IMAGE = "SS";
	
	// Login
	public static final String USERNAME = "Admin";
	public static final String PASSWORD = "admin123";
	public static final String INVALIDUSERNAME = "Admin1";
	public static final String INVALIDPASSWORD = "admin1234";
	
	// Dashboard
	public static final String TEXTDASBOARD = "Dashboard";
	
	// Markup
	public static final Markup PASS = MarkupHelper.createLabel("PASS", ExtentColor.GREEN);
	public static final Markup FAIL = MarkupHelper.createLabel("PASS", ExtentColor.RED);
}
