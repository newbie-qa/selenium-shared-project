package com.qa.newbie.util;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

public class ForceClose {
		private static ExtentTest test;
	public static void closeTest (WebDriver driver, ExtentReports extent) {
		test = extent.createTest("Test Interupted").fail("fail");
		test.info("Force Close the broser because error test");
		driver.close();
		driver.quit();
		extent.flush();
	}
}
