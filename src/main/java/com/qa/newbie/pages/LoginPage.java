package com.qa.newbie.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.qa.newbie.util.ElementActions;

public class LoginPage {

	ExtentReports extent = null;
	ExtentTest scenario = null;
	ExtentTest node = null;
	WebDriver driver;
	ElementActions elementActions;

	//Create OT/Page Object -> using by locator

	By usernameinputbox = By.name("username");
	By passwordinputbox = By.name("password");
	By loginbtn = By.xpath("/html/body/div/div[1]/div/div[1]/div/div[2]/div[2]/form/div[3]/button");
	By alertCredential = By.xpath("//*[contains(@class, \"oxd-text\") and text()='Invalid credentials']");

	// Define Constructor

	public LoginPage(WebDriver driver, ExtentReports extent) {
		this.driver = driver;
		this.extent = extent;
		elementActions = new ElementActions(driver);
	}

	//Page Action/Method
	public LoginPage verifyUsername(int timeout, int sleep) {
		elementActions.verifyELementVisible(usernameinputbox, timeout, sleep);
		return this;
	}

	public LoginPage setUsername(String text) {
		elementActions.doSendKey(usernameinputbox, text);
		return this;
	}

	public LoginPage verifyPassword(int timeout, int sleep) {
		elementActions.verifyELementVisible(passwordinputbox, timeout, sleep);
		return this;
	}

	public LoginPage setPassword(String text) {
		elementActions.doSendKey(passwordinputbox, text);
		return this;
	}

	public LoginPage verifyBtnLogin(int timeout, int sleep) {
		elementActions.verifyELementVisible(loginbtn, timeout, sleep);
		return this;
	}

	public LoginPage clickBtnLogin() {
		elementActions.doClick(loginbtn);
		return this;
	}

	public LoginPage verifyAlertCredential(int timeout, int sleep) {
		elementActions.verifyELementVisible(alertCredential, timeout, sleep);
		return this;
	}
}
