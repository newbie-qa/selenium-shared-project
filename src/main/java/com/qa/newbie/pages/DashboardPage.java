package com.qa.newbie.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.qa.newbie.util.Constant;
import com.qa.newbie.util.ElementActions;

public class DashboardPage {
	ExtentReports extent;
	WebDriver driver;
	ElementActions elementActions;
	
	//Create OT/Page Object -> using by locator
	By textDashboard = By.xpath("/html/body/div/div[1]/div[1]/header/div[1]/div[1]/span/h6");
	
	
	// Define Constructor
	
	public DashboardPage (WebDriver driver, ExtentReports extent) {
		this.driver = driver;
		this.extent = extent;
		elementActions = new ElementActions(driver);
	}
	
	//Page Action/Method
	
	public DashboardPage verifyTextDashboard(int timeout, int sleep) {
		elementActions.verifyELementVisible(textDashboard, timeout, sleep);
		return this;
	}
	
	public DashboardPage assertTextDashboard() {
		elementActions.verifyObject(textDashboard, Constant.TEXTDASBOARD);
		return this;
	}
}

